FROM node:12 AS base
WORKDIR /src
COPY package*.json /src/

FROM base AS prod
RUN npm i --production
COPY server/ /src/server/
COPY client/ /src/client/
EXPOSE 3001
CMD ["npm", "start"]

FROM prod AS dev
RUN npm i
CMD ["npm", "run", "devStart"]

FROM dev AS test
COPY test/ /src/test/
CMD ["npm", "test"]
